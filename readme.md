# Finartz 3D Secure Tester Challenge


## The API's For Left And Right
method: /finartz/v1/leftClient
```json
{
  "url": "http://localhost:8080/finartz/v1/rightServer",
  "mayKey": "mayVal"
}
 ```
- Example Response from Server
```json
{
    "url": "http://localhost:8080/finartz/v1/rightServer",
    "mayKey": "mayVal",
    "middleId": "1d112293-3a9e-4fd6-9e30-1f84812447b4",
    "rightId": "1"
}
``` 

## The Dev Diagram
![DevDiagram](DevDiagram.png)

Here:
 - The ``left(client)`` indicates a http request to ``middle``.
 - Middle setting a ``middleId`` and routing the request to ``right(server)``.
 - Right response to ``middle``. 
 - Middle routing the response to ``left(client)``.

## Test Diagram
![TestDiagram](TestDiagram.png)


## Technical Details
- I use Java 8, Spring Framework, Cucumber, RestAssured, JUnit, etc. 
- If you want the run cucumber tests, you should TestRunner run as JUnit


