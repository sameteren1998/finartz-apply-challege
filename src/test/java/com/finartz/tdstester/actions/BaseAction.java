package com.finartz.tdstester.actions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.runner.RunWith;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.RequestSpecification;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
public class BaseAction {
	com.jayway.restassured.response.Response response;
	RequestSpecification request;
	Map<String, Object> header = new HashMap<>();
	Map<String, Object> body = new HashMap<>();
	Map<String, Object> bugs = new HashMap<>();
	Map<String, Object> baseModel = new HashMap<>();


	@Before()
	public void clearOldDatas() {
		header.clear();
		body.clear();
		
		
	}


	@Given("^Set BaseUrl \"([^\"]*)\"$")
	public void set_baseurl(String URL) throws Throwable {
		RestAssured.baseURI = URL;

	}


	@Given("^Set Model$")
	public void set_model(DataTable data) throws Throwable {
		List<List<String>> list = data.asLists(String.class);
		for (int i = 0; i < list.size(); i++) {
			baseModel.put(list.get(i).get(0), list.get(i).get(1));
		}
	}

	@Given("^Set Header Accept \"([^\"]*)\" \"([^\"]*)\" And Content type \"([^\"]*)\" \"([^\"]*)\"$")
	public void set_header_accept_and_content_type(String acceptTypeKey, String acceptTypeValue, String contentTypeKey,
			String contentTypeValue) throws Throwable {
		header.put(acceptTypeKey, acceptTypeValue);
		header.put(contentTypeKey, contentTypeValue);
	}


	@Given("^Set Method \"([^\"]*)\"$")
	public void set_method(String methodName) throws Throwable {
		RestAssured.basePath = methodName;
	}

	
	@Given("^Set Body$")
	public void set_body(DataTable data) throws Throwable {
		List<List<String>> list = data.asLists(String.class);
		for (int i = 0; i < list.size(); i++) {
			body.put(list.get(i).get(0), list.get(i).get(1));
		}
	}
	
	@Then("^Post the method And Status Code is \"([^\"]*)\" $")
    public void post_the_method_and_status_code_is_something(String responseCode) throws Throwable {
		response = RestAssured.given().headers(header).body(body).post();
		if (response.getStatusCode() == Integer.parseInt(responseCode)) {
			
		} else {
			response.getStatusCode();
			String statusCode = String.valueOf(response.getStatusCode());
			// String responseBody = String.valueOf(response.getBody());
			Assert.fail("STATUS CODE = " + statusCode);

		}
    }

	@Then("^Post the method And Status Code is \"([^\"]*)\" And validate \"([^\"]*)\"$")
    public void post_the_method_and_status_code_is_something_and_validate_something(String responseCode, String variable) throws Throwable {
		response = RestAssured.given().headers(header).body(body).post();
		if (response.getStatusCode() == Integer.parseInt(responseCode) && response.jsonPath().getString(variable) != null) {
			
		} else {
			response.getStatusCode();
			String statusCode = String.valueOf(response.getStatusCode());
			// String responseBody = String.valueOf(response.getBody());
			Assert.fail("STATUS CODE = " + statusCode);

		}
    }

}
