Feature: Finartz Challenge Feature

Background:
Given Set BaseUrl "http://localhost:8080/finartz/v1"
Given Set Header Accept "Accept" "application/json" And Content type "Content-Type" "application/json"

@ApiTestFinartz
Scenario: Finartz Left Client Send A Message to Middle
Given Set Method "/leftClient"
Given Set Body
| url | http://localhost:8080/finartz/v1/rightServer |
| mayKey | mayVal |
Then Post the method And Status Code is "202" And validate "middleId"

@ApiTestFinartz
Scenario: Finartz Middle Send A Message to Server
Given Set Method "/rightServer"
Given Set Body
| url | http://localhost:8080/finartz/v1/rightServer |
| mayKey | mayVal |
When Post the method And Status Code is "202" 


