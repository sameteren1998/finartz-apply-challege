package com.finartz.tdstester.mock;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.reactive.function.BodyExtractor;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.finartz.tdstester.handler.LeftHandler;
import com.finartz.tdstester.request.LeftRequest;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

import reactor.core.publisher.Mono;

public class LeftHandlerMock {

	@Before
	public WebClient client() {
		return WebClient.builder().build();
	}

	@Test // As left: test if middleId received and it has the same value as with right
	public void testMiddleId() {

		String middleIdinMiddle = UUID.randomUUID().toString();
		LeftRequest leftRequest = new LeftRequest();
		leftRequest.setMayKey("Random");
		leftRequest.setUrl("http://localhost:8080/finartz/v1/rightServer");
		leftRequest.setMiddleId(middleIdinMiddle);
		Response response = RestAssured.given().header("Accept", "application/json")
				.header("Content-Type", "application/json").body(leftRequest)
				.post("http://localhost:8080/finartz/v1/rightServer");
		String middleIdinServer = response.jsonPath().getString("middleId");
		assertEquals(middleIdinMiddle, middleIdinServer);

	}



}
