package com.finartz.tdstester.response;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class LeftResponse {
	
	private String url;
	
	private String mayKey;
	
	private String middleId;
	
	private String rightId;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMayKey() {
		return mayKey;
	}
	public void setMayKey(String mayKey) {
		this.mayKey = mayKey;
	}
	public String getMiddleId() {
		return middleId;
	}
	public void setMiddleId(String middleId) {
		this.middleId = middleId;
	}
	public String getRightId() {
		return rightId;
	}
	public void setRightId(String rightId) {
		this.rightId = rightId;
	}
	
}
