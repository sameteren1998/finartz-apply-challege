package com.finartz.tdstester.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
@Getter
@Setter
public class RightRequest {

	private String middleId;
	private String mayKey;
	private String rightId;
	private String url;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMiddleId() {
		return middleId;
	}
	public String getRightId() {
		return rightId;
	}
	public void setRightId(String rightId) {
		this.rightId = rightId;
	}
	public void setMiddleId(String middleId) {
		this.middleId = middleId;
	}
	public String getMayKey() {
		return mayKey;
	}
	public void setMayKey(String mayKey) {
		this.mayKey = mayKey;
	}
	
	
}
