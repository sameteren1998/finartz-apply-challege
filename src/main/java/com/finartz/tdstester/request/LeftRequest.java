package com.finartz.tdstester.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class LeftRequest {

    private String url;
	
	private String mayKey;

	private String middleId;
	
	public String getMiddleId() {
		return middleId;
	}

	public void setMiddleId(String middleId) {
		this.middleId = middleId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMayKey() {
		return mayKey;
	}

	public void setMayKey(String mayKey) {
		this.mayKey = mayKey;
	}



	
}
