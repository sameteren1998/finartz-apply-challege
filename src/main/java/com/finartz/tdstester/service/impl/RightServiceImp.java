package com.finartz.tdstester.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.finartz.tdstester.request.RightRequest;
import com.finartz.tdstester.response.RightResponse;
import com.finartz.tdstester.service.RightService;
import com.jayway.restassured.response.Response;

@Service
public class RightServiceImp implements RightService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	public Response response;
    public int rightId = 0;

	@Override
	public RightResponse right(RightRequest request) {
		this.logger.info("This user create a request from RightServer to Middle : {}", request);
		RightResponse response = new RightResponse();
		response.setMayKey(request.getMayKey());
		response.setMiddleId(request.getMiddleId());
		response.setRightId(String.valueOf(rightId++));
		response.setUrl(request.getUrl());
		
		
		return response;
	}

}
