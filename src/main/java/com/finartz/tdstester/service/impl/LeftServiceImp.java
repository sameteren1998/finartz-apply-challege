package com.finartz.tdstester.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.finartz.tdstester.request.LeftRequest;
import com.finartz.tdstester.response.LeftResponse;
import com.finartz.tdstester.service.LeftService;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

@Service
public class LeftServiceImp implements LeftService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	public Response response;
	
	
	@Override
	public LeftResponse left(LeftRequest request) {
		Response response =  RestAssured.given()
				.header("Accept", "application/json")
				.header("Content-Type", "application/json")		
				.body(request)
				.post("http://localhost:8080/left");
		LeftResponse leftResponse = new LeftResponse();
		leftResponse.setMayKey(response.jsonPath().getString("mayKey"));
		leftResponse.setMiddleId(response.jsonPath().getString("middleId"));
		leftResponse.setRightId(response.jsonPath().getString("rightId"));
		leftResponse.setUrl(response.jsonPath().getString("url"));
		return leftResponse;
	}
	
	

}
