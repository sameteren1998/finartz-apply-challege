package com.finartz.tdstester.service;

import com.finartz.tdstester.request.LeftRequest;
import com.finartz.tdstester.response.LeftResponse;

public interface LeftService {

	LeftResponse left(LeftRequest request);
	
}
