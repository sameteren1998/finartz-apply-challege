package com.finartz.tdstester.service;

import com.finartz.tdstester.request.RightRequest;
import com.finartz.tdstester.response.RightResponse;

public interface RightService {

	RightResponse right(RightRequest request);
	
}
