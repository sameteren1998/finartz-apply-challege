package com.finartz.tdstester.handler;

import java.util.UUID;

import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.fasterxml.jackson.databind.node.ObjectNode;

import reactor.core.publisher.Mono;

public class LeftHandler implements HandlerFunction<ServerResponse> {

    private final WebClient client;
    private final static String ID_FIELD = "middleId";
    private final static String URL_FIELD = "url";
    public static String middleId;
    
    public LeftHandler(WebClient client) {
        this.client = client;
    }
 

    @Override
    public Mono<ServerResponse> handle(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(ObjectNode.class).flatMap(leftRequest -> {
            final String url = leftRequest.get(URL_FIELD).asText();
            leftRequest.put(ID_FIELD, UUID.randomUUID().toString());
            middleId =  leftRequest.get(ID_FIELD).toString();
            return client
                    .post()
                    .uri(url)
                    .bodyValue(leftRequest)
                    .exchange()
                    .flatMap(cr -> cr.bodyToMono(ObjectNode.class).flatMap(json -> {
                        return ServerResponse
                                .ok()
                                .contentType(MediaType.APPLICATION_JSON)
                                .bodyValue(json);
                    }));
        });
    }
   
}
