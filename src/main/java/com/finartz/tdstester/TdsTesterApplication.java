package com.finartz.tdstester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TdsTesterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TdsTesterApplication.class, args);
    }

}
