package com.finartz.tdstester.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.finartz.tdstester.request.LeftRequest;
import com.finartz.tdstester.response.LeftResponse;
import com.finartz.tdstester.service.LeftService;


@RequestMapping(value = "/finartz/v1", produces = "application/json", consumes = "application/json")
@RestController
public class LeftClient {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired 
	private LeftService leftService;
	
	
	@RequestMapping(value = "/leftClient", method = RequestMethod.POST)
	public ResponseEntity<LeftResponse> createCustomer(
			@RequestBody LeftRequest request) {
		this.logger.info("This user create a request from LeftClient to Middle : {}", request);
		LeftResponse response = leftService.left(request);	
		return ResponseEntity.accepted().body(response);
	}

	


}
