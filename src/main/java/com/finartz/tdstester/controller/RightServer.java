package com.finartz.tdstester.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.finartz.tdstester.request.RightRequest;
import com.finartz.tdstester.response.RightResponse;
import com.finartz.tdstester.service.RightService;


@RequestMapping(value = "/finartz/v1", produces = "application/json", consumes = "application/json")
@RestController
public class RightServer {
	
	@Autowired 
	private RightService rightService;
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	

	@RequestMapping(value = "/rightServer", method = RequestMethod.POST)
	public ResponseEntity<RightResponse> rightServer(
			@RequestBody RightRequest request) {
		this.logger.info("This user create a request from Middle to RightServer : {}", request);

		RightResponse response =  rightService.right(request);
		
		
		return ResponseEntity.accepted().body(response);
	}

	


}
